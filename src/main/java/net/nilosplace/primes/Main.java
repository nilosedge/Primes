package net.nilosplace.primes;

public class Main {

	public static void main(String args[]) {
		//String prime1 = "1634733645809253848443133883865090859841783670033092312181110852389333100104508151212118167511579";
		//String prime2 = "1900871281664822113126851573935413975471896789968515493666638539088027103802104498957191261465571";
		//System.out.println(prime1.charAt(1) * prime1.charAt(2));
		int c = 0;
		for(long i = 0; i <= 999; i++) {
			for(long k = i; k <= 999; k++) {
				String out = (i * k) + "";
				if(out.startsWith("152")) {
					System.out.println(i + " * " + k + " = " + (i * k));
					c++;
				}
			}
		}
		System.out.println("Count: " + c);
	}
}
