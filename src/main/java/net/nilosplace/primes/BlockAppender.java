package net.nilosplace.primes;

import java.io.*;
import java.util.BitSet;
import java.util.zip.*;

public class BlockAppender {

	public static void main(String[] args) {
		
		int blockSize = 1_000_000;
		
		BitSet final_plus_primes = new BitSet();
		BitSet final_minus_primes = new BitSet();
		
		for(int i = 0; i < 100; i++) {
			try {
				FileInputStream fin = new FileInputStream("data/" + i + ".data.gz");
				GZIPInputStream gzin = new GZIPInputStream(fin);
				ObjectInputStream in = new ObjectInputStream(gzin);
				BitSet file_plus_primes = (BitSet)in.readObject();
				BitSet file_minus_primes = (BitSet)in.readObject();
				in.close();
				
				for(int k = file_plus_primes.nextSetBit(0); k >= 0; k = file_plus_primes.nextSetBit(k + 1)) {
					final_plus_primes.set(k + (i * blockSize));
				}
				for(int k = file_minus_primes.nextSetBit(0); k >= 0; k = file_minus_primes.nextSetBit(k + 1)) {
					final_minus_primes.set(k + (i * blockSize));
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		try {
			System.out.println("Writing out: " + 0 + " file");
			FileOutputStream fos = new FileOutputStream("data/" + 0 + "new.data.gz");
			GZIPOutputStream gzos = new GZIPOutputStream(fos);
			ObjectOutputStream out = new ObjectOutputStream(gzos);

			out.writeObject(final_plus_primes);
			out.writeObject(final_minus_primes);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		

	}

}
