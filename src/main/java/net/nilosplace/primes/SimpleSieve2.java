package net.nilosplace.primes;

import java.io.*;
import java.util.*;
import java.util.zip.GZIPOutputStream;

public class SimpleSieve2 {



	public static void main(String[] args) {
		Date start = new Date();

		//boolean[] non_primes = new boolean[1000];
		BitSet primes = new BitSet();
		int blockSize = 1_000_000_000;

		int[] reverse_mask = new int[] {
				1, 7, 11, 13, 17, 19, 23, 29
		};
		int[] mask = new int[30];
		for(int i = 1; i < reverse_mask.length; i++) {
			mask[reverse_mask[i]] = i;
		}

		primes.set(0);

		for(int prime_idx = primes.nextClearBit(0); prime_idx >= 0 && prime_idx < blockSize * (8d / 30d); prime_idx = primes.nextClearBit(prime_idx + 1)) {
			
			int prime = (reverse_mask[prime_idx % 8] + ((prime_idx / 8) * 30));
			
			for(int p = prime; p < blockSize * (8d / 30d); p += prime) {

				int rem = mask[(p % 30)];
				int off = (p / 30);
				int off_idx = off * 8 + rem;

				primes.set(off_idx);

				//System.out.println("Remove: " + p + " O: " + off + " R: " + rem + " P: " + prime);
				
			}
			//int rem = reverse_mask[prime_idx % 30];
			//int off = prime_idx / 8;
			//long prime = (rem + off);
			//System.out.println("Prime: " + prime);
			
		}
		System.out.println(primes.cardinality());
		Date end = new Date();

		int c = 4;
		System.out.println(blockSize * (8d / 30d));
		for(int prime_idx = primes.nextClearBit(0); prime_idx >= 0 && prime_idx < blockSize * (8d / 30d); prime_idx = primes.nextClearBit(prime_idx + 1)) {
			
			int rem = reverse_mask[prime_idx % 8];
			int off = (prime_idx / 8) * 30;
			if(isPrime(rem + off)) {
				System.out.println("C: " + c + " " + (rem + off));
			} else {
				System.out.println("Prime Fail: " + (rem + off));
				System.exit(-1);
			}
			c++;
		}
//		System.out.println("Count0: " + set.cardinality() + " Len: " + set.length());
		System.out.println("Count1: " + c + " Time: " + (end.getTime() - start.getTime()));
//
		try {
			FileOutputStream fos = new FileOutputStream("data/" + 0 + ".simple.data.gz");
			GZIPOutputStream gzos = new GZIPOutputStream(fos);
			ObjectOutputStream out = new ObjectOutputStream(gzos);
			out.writeObject(primes);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	static boolean isPrime(int n)
	{
		// Corner case
		if (n <= 1)
			return false;

		// Check from 2 to n-1
		for (int i = 2; i < n; i++)
			if (n % i == 0)
				return false;

		return true;
	}

}
