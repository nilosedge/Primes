package net.nilosplace.primes;

import java.io.*;
import java.util.*;
import java.util.zip.GZIPOutputStream;

public class SimpleSieve {



	public static void main(String[] args) {
		Date start = new Date();

		boolean[] non_primes = new boolean[1_000_000_000];


		int[] reverse_mask = new int[] {
			1, 7, 11, 13, 17, 19, 23, 29
		};
		int[] mask = new int[30];
		for(int i = 1; i < reverse_mask.length; i++) {
			mask[reverse_mask[i]] = i;
		}
		
		non_primes[0] = true;
		non_primes[1] = true;

		for(int i = 0; i < non_primes.length; i++) {
			if(!non_primes[i]) {
				for(long x = (long)i * (long)i; x < non_primes.length; x += i) {
					non_primes[(int)x] = true;
				}
			}
		}
		Date end = new Date();

		BitSet set = new BitSet();

		int c = 0;
		for(int i = 0; i < non_primes.length; i++) {
			if(!non_primes[i]) {
				int offset = mask[i % 30];
				int idx = (i / 30) * 8;
				System.out.println("C: " + c + " " + (i));
				set.set(idx + offset);
				c++;
			}
		}
		
		System.out.println("Count1: " + c + " Time: " + (end.getTime() - start.getTime()));
		
		try {
			FileOutputStream fos1 = new FileOutputStream("data/" + 0 + ".compress.data.gz");
			GZIPOutputStream gzos1 = new GZIPOutputStream(fos1);
			ObjectOutputStream out1 = new ObjectOutputStream(gzos1);
			out1.writeObject(set);
			out1.close();

			FileOutputStream fos = new FileOutputStream("data/" + 0 + ".simple.data.gz");
			GZIPOutputStream gzos = new GZIPOutputStream(fos);
			ObjectOutputStream out = new ObjectOutputStream(gzos);
			out.writeObject(non_primes);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
