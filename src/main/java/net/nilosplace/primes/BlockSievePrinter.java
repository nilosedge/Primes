package net.nilosplace.primes;

import java.io.*;
import java.util.BitSet;
import java.util.zip.GZIPInputStream;

public class BlockSievePrinter {

	public static void main(String[] args) {
		int blockSize = 100;

		//int block = 27;
		long c = 2;
		for(int block = 0; block < 1; block++) {
			System.out.println(block);
			int offset = block * blockSize;

			BitSet file_plus_primes = null;
			BitSet file_minus_primes = null;

			try {
				FileInputStream fin = new FileInputStream("data/" + block + "new.data.gz");
				GZIPInputStream gzin = new GZIPInputStream(fin);
				ObjectInputStream in = new ObjectInputStream(gzin);
				file_plus_primes = (BitSet)in.readObject();
				file_minus_primes = (BitSet)in.readObject();
				in.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			c += printPrimes(file_plus_primes, file_minus_primes, offset);
			System.out.println("Total: " + c);
		}
		
		

	}

	public static int printPrimes(BitSet high, BitSet low, int offset) {
		int c = 2;
		int trace = 279545368;
		for(int i = 0; i <= Math.max(low.size(), high.size()) + 1; i++) {
			int base = ((i + offset) * 6);
			if(low.get(i)) {
				c++;
				if(c == trace || c == trace+1) System.out.println((i + offset) + " -> " + (base - 1) + " -> " + isPrime(base - 1) + " C: " + c);
			}
			if(high.get(i)) {
				c++;
				if(c == trace || c == trace+1) System.out.println((i + offset) + " -> " + (base + 1) + " -> " + isPrime(base + 1) + " C: " + c);
			}
		}
		//System.out.println("Count: " + c);

		return c;
	}

	static boolean isPrime(int n)
	{
		// Corner case
		if (n <= 1)
			return false;

		// Check from 2 to n-1
		for (int i = 2; i < n; i++)
			if (n % i == 0)
				return false;

		return true;
	}
}
