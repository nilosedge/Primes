package net.nilosplace.primes;

import java.io.*;
import java.util.BitSet;
import java.util.zip.*;

import net.nilosplace.process_display.ProcessDisplayHelper;

public class BlockSieveThread extends Thread {
	
	private int sieve_block = 0;
	private int blockSize = 0;
	
	public BlockSieveThread(int sieve_block, int blockSize) {
		this.sieve_block = sieve_block;
		this.blockSize = blockSize;
	}

	public void run() {

		System.out.println("SB: " + sieve_block);
		BitSet plus_primes = new BitSet(blockSize);
		BitSet minus_primes = new BitSet(blockSize);

		for(int fo = 0; fo <= sieve_block / blockSize; fo++) {

			long offset = (long)sieve_block * (long)blockSize;
			long f_offset = fo * (long)blockSize;

			BitSet file_plus_primes = null;
			BitSet file_minus_primes = null;

			try {
				System.out.println("Opening File: " + fo);
				FileInputStream fin = new FileInputStream("data/" + fo + ".data.gz");
				GZIPInputStream gzin = new GZIPInputStream(fin);
				ObjectInputStream in = new ObjectInputStream(gzin);
				file_plus_primes = (BitSet)in.readObject();
				file_minus_primes = (BitSet)in.readObject();
				in.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

			ProcessDisplayHelper ph = new ProcessDisplayHelper(10000);
			ph.startProcess("Block: " + sieve_block + " File: " + fo, blockSize);
			for(int i = 0; i < blockSize; i++) {
				long value = i + offset;

				long lim = (long)(Math.sqrt(value));

				if(fo == 0) {
					minus_primes.set(i);
					plus_primes.set(i);
				}
				for(int kdx = file_minus_primes.nextSetBit(0); kdx >= 0 && kdx < lim; kdx = file_minus_primes.nextSetBit(kdx + 1)) {
					if(!minus_primes.get(i) && !plus_primes.get(i)) break;
					long prime = ((kdx + f_offset) * 6l - 1l);
					long mod = (value % prime);
					if(mod == kdx + f_offset) {
						minus_primes.clear(i);
					}
					if(mod == prime - (kdx + f_offset)) {
						plus_primes.clear(i);
					}
				}
				for(int kdx = file_plus_primes.nextSetBit(0); kdx >= 0 && kdx < lim; kdx = file_plus_primes.nextSetBit(kdx + 1)) {
					if(!minus_primes.get(i) && !plus_primes.get(i)) break;
					long prime = ((kdx + f_offset) * 6l + 1l);
					long mod = (value % prime);
					if(mod == prime - (kdx + f_offset)) {
						minus_primes.clear(i);
					}
					if(mod == kdx + f_offset) {
						plus_primes.clear(i);
					}
				}
				ph.progressProcess();

			}
			ph.finishProcess();

		}

		try {
			System.out.println("Writing out: " + sieve_block + " file");
			FileOutputStream fos = new FileOutputStream("data/" + sieve_block + ".data.gz");
			GZIPOutputStream gzos = new GZIPOutputStream(fos);
			ObjectOutputStream out = new ObjectOutputStream(gzos);

			out.writeObject(plus_primes);
			out.writeObject(minus_primes);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	
	}
}
