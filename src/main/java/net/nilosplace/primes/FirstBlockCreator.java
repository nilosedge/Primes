package net.nilosplace.primes;

import java.io.*;
import java.util.*;
import java.util.zip.GZIPOutputStream;

import net.nilosplace.process_display.ProcessDisplayHelper;

public class FirstBlockCreator {

	public static void main(String[] args) {
	
		//LinkedHashMap<Integer, Integer> plus_primes = new LinkedHashMap<Integer, Integer>();
		//LinkedHashMap<Integer, Integer> minus_primes = new LinkedHashMap<Integer, Integer>();
		
		int limit = 100;
		
		Date start = new Date();

		BitSet plus_primes = new BitSet(limit);
		BitSet minus_primes = new BitSet(limit);

		plus_primes.set(1);
		minus_primes.set(1);
		int i = 0;
		ProcessDisplayHelper ph = new ProcessDisplayHelper(10000);
		ph.startProcess("Primes", limit);
		for(i = 2; i < limit; i++) {
			
			int lim = (int)(Math.sqrt(i));
			
			minus_primes.set(i);
			plus_primes.set(i);
			for(int kdx = minus_primes.nextSetBit(0); kdx > 0 && kdx < lim; kdx = minus_primes.nextSetBit(kdx + 1)) {
				if(!minus_primes.get(i) && !plus_primes.get(i)) break;
				int prime = (kdx * 6 - 1);
				int mod = (i % prime);
				if(mod == kdx) {
					minus_primes.clear(i);
				}
				if(mod == prime - kdx) {
					plus_primes.clear(i);
				}
			}
			for(int kdx = plus_primes.nextSetBit(0); kdx > 0 && kdx < lim; kdx = plus_primes.nextSetBit(kdx + 1)) {
				if(!minus_primes.get(i) && !plus_primes.get(i)) break;
				int prime = (kdx * 6 + 1);
				int mod = i % prime;
				if(mod == prime - kdx) {
					minus_primes.clear(i);
				}
				if(mod == kdx) {
					plus_primes.clear(i);
				}
			}

			ph.progressProcess();
		}
		ph.finishProcess();
		
		Date end = new Date();
		System.out.println();

		int count = plus_primes.cardinality() + minus_primes.cardinality();
		
		try {
			//System.out.println(i);
			FileOutputStream fos = new FileOutputStream("data/0.data.gz");
			GZIPOutputStream gzos = new GZIPOutputStream(fos);
	        ObjectOutputStream out = new ObjectOutputStream(gzos);
	        out.writeObject(plus_primes);
			out.writeObject(minus_primes);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
        
		System.out.println(count + " ppms: " + (count / (end.getTime() - start.getTime())));
	}

}
