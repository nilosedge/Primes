package net.nilosplace.primes.nn;

import java.io.*;
import java.math.BigInteger;
import java.util.*;

public class ConvertFiles {

	public static void main(String[] args) throws Exception {
		File file = new File ("keys/primes4.txt");

		BigInteger n = null;
		List<BigInteger> numbers = new ArrayList<BigInteger>();

		Scanner scan = new Scanner(file);
		while (scan.hasNext()) {
			n = scan.nextBigInteger();
			numbers.add(n);
		}
		scan.close();

		//Writer w = new FileWriter("keys/primes3.txt");

		//ObjectFileSto
		
		FileOutputStream fos = new FileOutputStream("keys/primes5.txt.gz");
		ParallelGZIPOutputStream pgzos = new ParallelGZIPOutputStream(fos);
		BufferedOutputStream bos = new BufferedOutputStream(pgzos);


		System.out.println(numbers.size());

		for(int i = 0; i < numbers.size(); i++) {
			Date start = new Date();
			for(int k = i + 1; k < numbers.size(); k++) {
				
				bos.write(new String(numbers.get(i) + " " + numbers.get(k) + " " + numbers.get(i).multiply(numbers.get(k)) + "\n").getBytes());

			}
			bos.flush();
			Date end = new Date();
			System.out.println("Pass: " + i + " " + (end.getTime() - start.getTime()));
		}

		bos.close();
	}
}
