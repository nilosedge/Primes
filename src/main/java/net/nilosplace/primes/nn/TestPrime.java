package net.nilosplace.primes.nn;

import java.math.BigInteger;

public class TestPrime {

	public static void main(String[] args) throws Exception {
		ModelUtil primeModel = new ModelUtil("primes", false);
		
		String RSA = "135066410865995223349603216278805969938881475605667027524485143851526510604859533833940287150571909441798207282164471551373680419703964191743046496589274256239341020864383202110372958725762358509643110564073501508187510676594629205563685529475213500852879416377328533906109750544334999811150056977236890927563";
		String N = "132296507392594033336390283835257989328579887577878104417791129642982017433686226711403004329826583047346055238584235584007779432198571110261739030485656334925185459718098359582269856705539017292795452278946574742038561454638635198266529058672675500918328042057827552788841661274284544645256611578141541157543";
		
		Prime prime = new Prime(RSA + " " + RSA + " " + RSA);
		
		double[][] prediction = primeModel.getOutput(prime.getNArray());
		
		BigInteger P = BigInteger.ZERO;
		BigInteger Q = BigInteger.ZERO;
		
		for(int i = 0; i < prediction[0].length; i++) {
			//System.out.println(prediction[0][i]);
			if(prediction[0][i] > 0.5) {
				P = P.setBit(i);
			}
			if(prediction[1][i] > 0.5) {
				Q = Q.setBit(i);
			}
			//System.out.println(P.bitLength());
		}
		
		System.out.println(P);
		System.out.println(Q);
		System.out.println(P.multiply(Q));
		System.out.println(N);

		//NNPrimes.printArray("P", prediction[0]);
		//NNPrimes.printArray("Q", prediction[1]);
		
	}

}
