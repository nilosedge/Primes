package net.nilosplace.primes.nn;

import java.io.Serializable;
import java.nio.file.*;
import java.util.*;

import org.deeplearning4j.core.storage.StatsStorage;
import org.deeplearning4j.nn.api.Model;
import org.deeplearning4j.nn.conf.*;
import org.deeplearning4j.nn.conf.layers.*;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.api.BaseTrainingListener;
import org.deeplearning4j.ui.api.UIServer;
import org.deeplearning4j.ui.model.stats.StatsListener;
import org.deeplearning4j.ui.model.storage.InMemoryStatsStorage;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.evaluation.classification.*;
import org.nd4j.evaluation.regression.RegressionEvaluation;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.MultiDataSet;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.learning.config.Adam;
import org.nd4j.linalg.lossfunctions.LossFunctions.LossFunction;

import com.google.common.base.Joiner;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class ModelUtil {

	@Getter
	//private MultiLayerNetwork model;
	private ComputationGraph model;

	private String basePath = System.getProperty("user.dir");
	private String pathToModels = "models";
	private String modelName = "";

	private String modelIteration = String.format("%06d", 0);
	private String modelEpoch = String.format("%06d", 0);

	private UIServer uiServer;
	private StatsStorage statsStorage;
	
	@Getter
	private List<Prime> primes = new ArrayList<>();

	public ModelUtil(String modelName, boolean startServer) throws Exception {
		statsStorage = new InMemoryStatsStorage();
		
		if(startServer) {
			uiServer = UIServer.getInstance();
			uiServer.attach(statsStorage);
			log.info(uiServer.getAddress());
		}
		
		if(modelName != null && modelName.length() > 0) {
			this.modelName = modelName;
			readModel(getLatestModelFile());
		} else {
			log.error("Can not load model: " + modelName);
		}
	}
	
	public void saveModel() throws Exception {
		saveModel(getFullPath());
	}

	private void saveModel(String modelFile) throws Exception {
		log.info("Saving Model: " + modelFile);
		ModelSerializer.writeModel(model, modelFile, true);	
	}
	private void readModel(String modelFile) throws Exception {
		log.info("Loading Model: " + modelFile);

		model = ModelSerializer.restoreComputationGraph(modelFile);
		//model = ModelSerializer.restoreMultiLayerNetwork(modelFile);
		model.setListeners(new StatsListener(statsStorage), new ScoreListener());
	}

	public void initModel() throws Exception {

		ComputationGraphConfiguration.GraphBuilder conf;
		conf =  new NeuralNetConfiguration.Builder()
				.l2(0.0001)
				//.seed(1234567)
				.updater(new Adam(0.0001))
				.weightInit(WeightInit.XAVIER)
				//.cacheMode(CacheMode.DEVICE)
				.graphBuilder();
		
		conf.addInputs("input");
		
		conf.addLayer("dense1", new DenseLayer.Builder().nIn(1024).nOut(1024).activation(Activation.RELU).build(), "input");
		conf.addLayer("dense2", new DenseLayer.Builder().nIn(1024).nOut(1024).activation(Activation.RELU).build(), "dense1");
		//conf.addLayer("dense3", new DenseLayer.Builder().nIn(36).nOut(36).activation(Activation.RELU).build(), "dense2");
		//conf.addLayer("dense4", new DenseLayer.Builder().nIn(36).nOut(36).activation(Activation.RELU).build(), "dense3");
		
		

		conf.addLayer("dense5", new DenseLayer.Builder().nIn(1024).nOut(1024).activation(Activation.RELU).build(), "dense2");
		conf.addLayer("prime1_output", new OutputLayer.Builder(LossFunction.XENT).activation(Activation.SIGMOID).nIn(1024).nOut(512).build(), "dense5");
		

		conf.addLayer("dense6", new DenseLayer.Builder().nIn(1024).nOut(1024).activation(Activation.RELU).build(), "dense2");
		conf.addLayer("prime2_output", new OutputLayer.Builder(LossFunction.XENT).activation(Activation.SIGMOID).nIn(1024).nOut(512).build(), "dense6");
		
		
		conf.setOutputs("prime1_output", "prime2_output");
		
		model = new ComputationGraph(conf.build());

		model.init();
		
		
		log.info("Batch Size: " + model);
		log.info("Batch Size: " + model.getEpochCount());

	}


	private String getLatestModelFile() throws Exception {

		String fullPath = Joiner.on("/").join(basePath, pathToModels);

		Path brainPath = Paths.get(fullPath);
		DirectoryStream<Path> paths = null;

		String latestModelPath = null;
		try {
			paths = Files.newDirectoryStream(brainPath);
			TreeSet<String> pathSet = new TreeSet<String>();
			paths.forEach(p -> {
				pathSet.add(p.getFileName().toString());
			});
			//set.addAll();
			for(String path: pathSet) {
				String[] arr = path.split("-");
				if(arr[0].equals("model") && arr[1].equals(modelName)) {
					modelIteration = arr[2];
					modelEpoch = arr[3];
					latestModelPath = getFullPath();
					//modelIteration += 1;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		if(latestModelPath == null) {
			initModel();
			latestModelPath = getFullPath();
			saveModel(latestModelPath);
		}

		return latestModelPath;
	}

	private String getFullPath() {
		return Joiner.on("/").join(basePath, pathToModels, Joiner.on("-").join("model", modelName, modelIteration, modelEpoch));
	}

	private class ScoreListener extends BaseTrainingListener implements Serializable {

		Date start = new Date();
		Date end = start;
		
		@Override
		public void iterationDone(Model model, int iteration, int epoch) {
			end = new Date();
			modelIteration = String.format("%06d", iteration);
			modelEpoch = String.format("%06d", epoch);
			//double score = model.score();
			//log.info("Score at iteration I: {} E: {} S: {}", iteration, epoch, score);
			try {
				if(end.getTime() - start.getTime() > 300000) {
					start = end;
					saveModel(getFullPath());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onEpochStart(Model model) {
			//log.debug("Epoch Start: ");
		}


		@Override
		public void onEpochEnd(Model model) {
			//log.debug("Epoch End: ");
		}

		//		@Override
		//		public String toString(){
		//			return "ScoreIterationListener(" + modelIteration + ")";
		//		}
	}
	
	
	
	public void addTraining(Prime prime) {
		this.primes.add(prime);
	}
	
	public void addTrainings(List<Prime> primes) {
		this.primes.addAll(primes);
	}

	public void eval() {
		evalPrimeP();
		evalPrimeQ();
	}
	
	
	public void train() {
		//log.info("Stats: " + stats.size());
		if(NNPrimes.train && primes.size() >= 10000) {
			log.info("Training with: " + primes.size() + " primes");
			Collections.shuffle(primes, NNPrimes.rand);

			double[][] NArr = new double[primes.size()][];
			double[][] QArr = new double[primes.size()][];
			double[][] PArr = new double[primes.size()][];
	
			int c = 0;
			for(Prime prime: primes) {
				NArr[c] = prime.getNArray();
				PArr[c] = prime.getPArray();
				QArr[c] = prime.getQArray();
				c++;
			}

			INDArray[] NData = new INDArray[] { Nd4j.create(NArr) };
			
			INDArray[] primeData = new INDArray[] { Nd4j.create(PArr), Nd4j.create(QArr) };
			

			MultiDataSet ds0 = new MultiDataSet(NData, primeData);
			//MultiDataSet ds1 = new MultiDataSet(gameStates, desiredPolicy);
			//MultiDataSet ds2 = new MultiDataSet(gameStates, desiredValue);
			
			//MultiDataSetIterator dsi = null;
			
			//DataNormalization normalizer = new NormalizerStandardize();
			//normalizer.fit(ds);
			//normalizer.transform(ds);
			
			//log.info(desiredData.shapeInfoToString());
			
			
			
			//for(int i = 0; i < 50; i++) {
				model.fit(ds0);
			//}
				
				
//				log.info(modelUtil.getModel().getLayer("dense1").getGradientsViewArray());
//				log.info(modelUtil.getModel().getLayer("dense2").getGradientsViewArray());
//				log.info(modelUtil.getModel().getLayer("dense3").getGradientsViewArray());
//				log.info(modelUtil.getModel().getLayer("policy_output").getGradientsViewArray());
//				log.info(modelUtil.getModel().getLayer("value_output").getGradientsViewArray());
				
			//log.info("Training: " + c + " moves finished");
			//eval();
			//modelUtil.getModel().incrementEpochCount();

			primes.clear();
		} else {
			//log.info("Stats: " + stats.size());
		}
	}

	public void evalPrimeP() {
		double[][] primeP_Ar = new double[primes.size()][];
		double[][] primeP_PredictionsAr = new double[primes.size()][];
		
		int c = 0;
		for(Prime prime: primes) {
			primeP_Ar[c] = prime.getPArray();
			//NNPrimes.printArray("P", primeP_Ar[c]);
			primeP_PredictionsAr[c] = prime.getPprediction();
			//NNPrimes.printArray("PP", primeP_PredictionsAr[c]);
			c++;
		}
		
		log.info("Evaluating: " + c + " primes");

		EvaluationBinary eval = new EvaluationBinary(512, 2);

		eval.eval(Nd4j.create(primeP_Ar), Nd4j.create(primeP_PredictionsAr));

		log.info(eval.stats());

	}
	
	public void evalPrimeQ() {

		double[][] primeQ_Ar = new double[primes.size()][];
		double[][] primeQ_PredictionsAr = new double[primes.size()][];
		
		int c = 0;
		for(Prime prime: primes) {
			primeQ_Ar[c] = prime.getQArray();
			//NNPrimes.printArray("Q", primeQ_Ar[c]);
			primeQ_PredictionsAr[c] = prime.getQprediction();
			//NNPrimes.printArray("QP", primeQ_PredictionsAr[c]);
			c++;
		}
		log.info("Evaluating: " + c + " primes");


		EvaluationBinary eval = new EvaluationBinary(512, 2);

		eval.eval(Nd4j.create(primeQ_Ar), Nd4j.create(primeQ_PredictionsAr));

		log.info(eval.stats());
	}
	
	
	
	public double[][] getOutput(double[] N) {
		INDArray primeNData = Nd4j.create(new double[][] {N});

		INDArray[] prediction = model.output(primeNData);
		
		double[] primeP = new double[(int)prediction[0].length()];
		for(int i = 0; i < prediction[0].length(); i++) { 
			primeP[i] = prediction[0].getDouble(i);
		}
		
		double[] primeQ = new double[(int)prediction[1].length()];
		for(int i = 0; i < prediction[1].length(); i++) { 
			primeQ[i] = prediction[1].getDouble(i);
		}
		
		double[][] ret = new double[2][];
		ret[0] = primeP;
		ret[1] = primeQ;
		
		return ret;
	}
	
	
	
	
	
	
	
	
}
