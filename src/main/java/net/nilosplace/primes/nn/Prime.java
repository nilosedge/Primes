package net.nilosplace.primes.nn;

import java.math.BigInteger;

import lombok.Data;

@Data
public class Prime {
	private BigInteger N;
	private BigInteger P;
	private BigInteger Q;
	private double[] Pprediction;
	private double[] Qprediction;

	public Prime(String line) {
		String[] array = line.split(" ");
		P = new BigInteger(array[0]);
		Q = new BigInteger(array[1]);
		N = new BigInteger(array[2]);
	}
	
	public double[] getNArray() {
		double[] ret = new double[N.bitLength()];
		for(int i = 0; i < N.bitLength(); i++) {
			if(N.testBit(i)) {
				ret[i] = 1.0;
			}
		}
		//NNPrimes.printArray("N", ret);
		return ret;
	}
	
	public double[] getPArray() {
		double[] ret = new double[P.bitLength()];
		for(int i = 0; i < P.bitLength(); i++) {
			if(P.testBit(i)) {
				ret[i] = 1.0;
			}
		}
		//NNPrimes.printArray("P", ret);
		return ret;
	}
	
	public double[] getQArray() {
		double[] ret = new double[Q.bitLength()];
		for(int i = 0; i < Q.bitLength(); i++) {
			if(Q.testBit(i)) {
				ret[i] = 1.0;
			}
		}
		//NNPrimes.printArray("Q", ret);
		return ret;
	}

	public void setPrediction(double[][] prediction) {
		Pprediction = prediction[0];
		for(int i = 0; i < Pprediction.length; i++) {
			if(Pprediction[i] < .5) Pprediction[i] = 0.0;
			else Pprediction[i] = 1.0;
		}
		//NNPrimes.printArray("PP", Pprediction);
		Qprediction = prediction[1];
		for(int i = 0; i < Qprediction.length; i++) {
			if(Qprediction[i] < .5) Qprediction[i] = 0.0;
			else Qprediction[i] = 1.0;
		}
		//NNPrimes.printArray("QP", Qprediction);
	}

}
