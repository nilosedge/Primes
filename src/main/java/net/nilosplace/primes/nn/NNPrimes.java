package net.nilosplace.primes.nn;

import java.io.*;
import java.util.Random;
import java.util.zip.GZIPInputStream;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class NNPrimes {

	public static boolean train = true;
	public static Random rand = new Random();

	public static void main(String[] args) throws Exception {
		new NNPrimes();
	}

	public NNPrimes() throws Exception {

		ModelUtil primeModel = new ModelUtil("primes", false);

		File file = new File ("keys/primes5.txt.gz");
		InputStream fileStream = new FileInputStream(file);
		InputStream gzipStream = new GZIPInputStream(fileStream);
		Reader decoder = new InputStreamReader(gzipStream);
		BufferedReader buffered = new BufferedReader(decoder);
		
		
		long c = 0;
		String line = null;
		while((line = buffered.readLine()) != null) {
			
			//if(c >= 9730000L) {
				Prime prime = new Prime(line);
				double[][] prediction = primeModel.getOutput(prime.getNArray());
				//NNPrimes.printArray("PrimeP", prediction[0]);
				prime.setPrediction(prediction);
				primeModel.addTraining(prime);
				
				//primeModel.train();
			//}

			if(c % 10000 == 0 && c > 0) {
				primeModel.eval();
				primeModel.getPrimes().clear();
				log.info(c);
			}
			c++;
		}
		
		

	}

	public static void printArray(String name, double[] array) {
		String ret = name + ": [";
		String delim = "";
		for(int i = 0; i < array.length; i++) {
			ret += delim + array[i];
			delim = " ";
		}
		ret += "]";
		log.info(ret);
	}




}
