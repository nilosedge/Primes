package net.nilosplace.primes.javafx;

import java.math.BigInteger;

import javafx.application.Application;
import javafx.event.*;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import lombok.*;

public class Main extends Application {

	private Label[][] labels;
	private Label[] resultLabels;
	private Label[] answerLabels;
	
	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		String javaVersion = System.getProperty("java.version");
		String javafxVersion = System.getProperty("javafx.version");

		//int i1 = 719;
		//int i2 = 839;
		
		BigInteger N = new BigInteger("671998030559713968361666935769");
		
		Primes primes = new Primes();
		Label label1 = new Label("");
		label1.setFont(new Font("Courier", 10));
		Label label2 = new Label(N.toString(2));
		label2.setFont(new Font("Courier", 10));
		
		GridPane gridPane = new GridPane();
		int len = N.toString(2).length();
		
		for(int i = len; i >= 0 ; i--) {
			CheckBox c1 = new CheckBox();
			c1.setUserData(new UserData(i, primes));

			EventHandler h1 = new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					UserData data = (UserData) c1.getUserData();
					if(c1.isSelected()) {
						data.getPrimes().setPrime1(data.getPrimes().getPrime1().setBit(data.getIdx()));
					} else {
						data.getPrimes().setPrime1(data.getPrimes().getPrime1().clearBit(data.getIdx()));
					}
					label1.setText(
							primes.getPrime1().multiply(primes.getPrime2()).toString(2) + " " +
							primes.getPrime1() + " * " + primes.getPrime2() + " = " +
							primes.getPrime1().multiply(primes.getPrime2()));
					changeLabels(primes, N);
					System.out.println("Prime1: " + data.getPrimes().getPrime1());
				}};
			
			c1.addEventHandler(ActionEvent.ANY, h1);
			gridPane.add(c1, (len + 2) - i, ((len + 2) - i) + len);
			
			CheckBox c2 = new CheckBox();
			c2.setUserData(new UserData(i, primes));

			EventHandler h2 = new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					UserData data = (UserData) c2.getUserData();
					if(c2.isSelected()) {
						data.getPrimes().setPrime2(data.getPrimes().getPrime2().setBit(data.getIdx()));
					} else {
						data.getPrimes().setPrime2(data.getPrimes().getPrime2().clearBit(data.getIdx()));
					}
					label1.setText(
							primes.getPrime1().multiply(primes.getPrime2()).toString(2) + " " +
							primes.getPrime1() + " * " + primes.getPrime2() + " = " +
							primes.getPrime1().multiply(primes.getPrime2()));
					changeLabels(primes, N);
					System.out.println("Prime2: " + data.getPrimes().getPrime2());
				}
			};
			
			c2.addEventHandler(ActionEvent.ANY, h2);

			gridPane.add(c2, ((len + 2) - i), i);
		}

		setupLabels(gridPane, N);

		//gridPane.add(label1, 0, 21);
		//gridPane.add(label2, 0, 22);
		

		Scene scene = new Scene(gridPane, 512, 740);
		primaryStage.setScene(scene);
		primaryStage.show();

	}
	

	private void changeLabels(Primes primes, BigInteger N) {
		int size = (int)Math.ceil(N.toString(2).length() / 2);
		for(int i = 0; i < size; i++) {
			for(int k = 0; k < size; k++) {
				if(primes.getPrime1().testBit(i) && primes.getPrime2().testBit(k)) {
					labels[(size - 1) - i][(size - 1) - k].setText("1");
				} else {
					labels[(size - 1) - i][(size - 1) - k].setText("0");
				}
			}
		}
		BigInteger result = primes.getPrime1().multiply(primes.getPrime2());
		int fullSize = N.toString(2).length();
		for(int i = 0; i < fullSize; i++) {
			if(result.testBit(i)) resultLabels[(fullSize - 1) - i].setText("1");
			else resultLabels[(fullSize - 1) - i].setText("0");
		}
	}
	
	private void setupLabels(GridPane gridPane, BigInteger N) {
		int size = (int)Math.ceil(N.toString(2).length() / 2);
		labels = new Label[size][size];
		
		int c = 0;
		for(int i = 0; i < size; i++) {
			for(int k = 0; k < size; k++) {
				labels[i][k] = new Label("0");
				labels[i][k].setFont(new Font("Courier", 10));
				gridPane.add(labels[i][k], i + k + 3, (size - k) + i);
				c++;
			}
		}
		int len = N.toString(2).length();
		resultLabels = new Label[len];
		answerLabels = new Label[len];
		//System.out.println(len);
		//System.out.println();
		for(int i = 0; i < len; i++) {
			resultLabels[i] = new Label("0");
			resultLabels[i].setFont(new Font("Courier", 10));
			answerLabels[i] = new Label("0");
			answerLabels[i].setFont(new Font("Courier", 10));
			
			System.out.println((len - 1) - i);
			if(N.testBit((len - 1) - i)) answerLabels[i].setText("1");
			gridPane.add(resultLabels[i], i + 2, N.toString(2).length());
			gridPane.add(answerLabels[i], i + 2, N.toString(2).length() + 1);
		}
		
		
	}

	@Data
	public class Primes {
		private BigInteger prime1 = BigInteger.ZERO;
		private BigInteger prime2 = BigInteger.ZERO;
	}
	
	@Data @AllArgsConstructor
	public class UserData {
		private int idx;
		private Primes primes;
	}

}
