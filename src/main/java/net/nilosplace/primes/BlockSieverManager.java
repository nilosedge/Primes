package net.nilosplace.primes;

import java.util.concurrent.*;

public class BlockSieverManager {

	public static void main(String[] args) {

		ExecutorService service = Executors.newFixedThreadPool(6);

		//int sieve_block = 58;
		int blockSize = 100;

		for(int sieve_block = 1; sieve_block < 10; sieve_block++) {
			BlockSieveThread2 thread = new BlockSieveThread2(sieve_block, blockSize);

		//for(int sieve_block = 40; sieve_block < 50; sieve_block++) {
		//	BlockSieveThread thread = new BlockSieveThread(sieve_block, blockSize);

			service.execute(thread);
		}
		service.shutdown();
		try {
			System.out.println("Waiting for threads to finish");
			service.awaitTermination(1, TimeUnit.DAYS);
			System.out.println("Threads finished");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}



}
