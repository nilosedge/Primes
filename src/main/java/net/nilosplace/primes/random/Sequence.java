package net.nilosplace.primes.random;

import lombok.Data;

@Data
public class Sequence {
	public long position;
	public long size;
}
