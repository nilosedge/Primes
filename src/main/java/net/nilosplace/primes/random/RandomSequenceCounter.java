package net.nilosplace.primes.random;

import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class RandomSequenceCounter {

	public static void main(String[] args) {
		Random r = new Random();
	
		List<Sequence> falseList = new ArrayList<>();
		List<Sequence> trueList = new ArrayList<>();
		
		boolean last = false;
		long counter = 0;
		Sequence currentSequence = null;
		while(counter++ < 100000000l) {
			boolean bool = r.nextInt(100) > 15;
			if(bool == last && currentSequence != null) {
				currentSequence.size++;
			} else {
				currentSequence = new Sequence();
				currentSequence.position = counter;
				currentSequence.size = 1;
				if(bool) trueList.add(currentSequence);
				else falseList.add(currentSequence);
			}
			last = bool;
		}
		
		Map<Long, List<Sequence>> trueGroups = trueList.stream()
				  .collect(Collectors.groupingBy(Sequence::getSize));
		for(Entry<Long, List<Sequence>> entry: trueGroups.entrySet()) {
			
			System.out.println(entry.getKey() + ": " + entry.getValue().size());
		}
		
		Map<Long, List<Sequence>> falseGroups = falseList.stream()
				  .collect(Collectors.groupingBy(Sequence::getSize));
		for(Entry<Long, List<Sequence>> entry: falseGroups.entrySet()) {
			
			System.out.println(entry.getKey() + ": " + entry.getValue().size());
		}
		
		//System.out.println(trueGroups);
		//System.out.println(falseList);
	}
	
}
