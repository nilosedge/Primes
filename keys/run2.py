import os
from Crypto.PublicKey import RSA

o = open("primes.txt", "a")

for i in range(1, 10001):
	f = open("file" + str(i), "r")
	key = RSA.importKey(f.read())

	o.write(str(key.q) + "\n")
	o.write(str(key.p) + "\n")
	#print(key.q)
	#print(key.p)
	f.close()
	os.remove("file" + str(i))
	os.remove("file" + str(i) + ".pub")
o.close()
